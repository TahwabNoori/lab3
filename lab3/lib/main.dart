//Authors: Tahwab Noori and Carlitos Carmona
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}
class ReviewChip extends StatefulWidget {
  ReviewChip({this.reviewText});
  final String reviewText;

  @override
  State<StatefulWidget> createState(){
    return ReviewState(
      reviewText: this.reviewText
    );
  }
}

class ReviewState extends State<ReviewChip> {
  ReviewState({this.reviewText});
  final String reviewText;
  bool _isSelected = false;

  @override
  Widget build(BuildContext context) {
    return InputChip(
      label: Text(reviewText, style: _isSelected ?
                                TextStyle(color: Colors.white) : TextStyle(color: Colors.black)),
      backgroundColor: Colors.white60,
      checkmarkColor: Colors.white,
      elevation: 5,
      pressElevation: 10,
      selectedColor: Colors.orangeAccent,
      selected: _isSelected,
      onSelected: (bool newValue) {
        setState(() {
          _isSelected = newValue;
        });
      },
    );
  }
}
class ReviewWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.mode_edit, color: Colors.orangeAccent,),
                  title: Text('Leave us a review!'),
                ),
                Row(
                    children: <Widget>[
                      ReviewChip(reviewText: "Good food"),
                      const SizedBox(width: 8),
                      ReviewChip(reviewText: "Great vibe"),
                      const SizedBox(width: 8),
                      ReviewChip(reviewText: "Authentic feel",)
                    ]
                ),
                Row(
                    children: <Widget>[
                      ReviewChip(reviewText: "Amazing prices"),
                      const SizedBox(width: 8),
                      ReviewChip(reviewText: "Friendly service"),
                      const SizedBox(width: 8),
                    ]
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    OutlineButton(
                      child: const Text('Submit'),
                      highlightColor: Colors.orangeAccent,
                      highlightedBorderColor: Colors.white,
                      splashColor: Colors.orangeAccent,
                      onPressed: () {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text("Thanks for your submission!", style: TextStyle(fontSize: 20),),
                            backgroundColor: Colors.orangeAccent,
                            duration: Duration(seconds: 2),
                          )
                        );
                      },
                    ),
                    const SizedBox(width: 8),
                  ],
                ),
              ],
            ),
          ),
        )
    );
  }
}

/// This is the stateless widget that contains the modal sheet.
class HoursWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Day',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Hours',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Delivery',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Sunday')),
            DataCell(Text('10AM-8PM')),
            DataCell(Text('10AM-9:30PM')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Monday')),
            DataCell(Text('8AM-10PM')),
            DataCell(Text('8AM-10:30PM')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Tuesday')),
            DataCell(Text('8AM-10PM')),
            DataCell(Text('8AM-10:30PM')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Wednesday')),
            DataCell(Text('8AM-10PM')),
            DataCell(Text('8AM-10:30PM')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Thursday')),
            DataCell(Text('8AM-10PM')),
            DataCell(Text('8AM-10:30PM')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Friday')),
            DataCell(Text('9AM-12AM')),
            DataCell(Text('9AM-2AM')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Saturday')),
            DataCell(Text('9AM-12AM')),
            DataCell(Text('9AM-2AM')),
          ],
        ),
      ],
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;

  static List<Widget> _widgetOptions = <Widget>[
    ReviewWidget(),
    HoursWidget(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Carlitos Taqueria'),
        backgroundColor: Colors.orangeAccent,
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.mode_edit,),
            label: 'Review Us',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.access_time),
            label: 'Schedule',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
